# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=release-plz
pkgver=0.3.84
pkgrel=0
pkgdesc="Release Rust packages without using the command line"
url="https://github.com/MarcoIeni/release-plz"
license="MIT OR Apache-2.0"
arch="all"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	openssl-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/MarcoIeni/release-plz/archive/release-plz-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgname-v$pkgver"

prepare() {
	default_prepare
	cargo fetch --locked
}

check() {
	RUST_MIN_STACK=8388608 cargo test --frozen --no-default-features -- \
		--skip "project" \
		--skip "input_generates_correct_release_request"
}

build() {
	OPENSSL_NO_VENDOR=1 cargo auditable build -p "$pkgname" --release --frozen
	mkdir -p completions/
	local compgen="target/release/$pkgname generate-completions"
	$compgen bash >"completions/$pkgname"
	$compgen fish >"completions/$pkgname.fish"
	$compgen zsh >"completions/_$pkgname"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE-MIT -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm 644 "completions/$pkgname" -t "$pkgdir/usr/share/bash-completion/completions/"
	install -Dm 644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d/"
	install -Dm 644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions/"
}

sha512sums="
ac1df84eb0fb7d9a93af953a1c577d4c3d651bcf41491f8939bee76a9dfe2c39e3be1aaf0ac6d3fd74063ff7af67d8c5328ba2663a1b5de1d80dec75f690c73e  release-plz-v0.3.84.tar.gz
"
